<?php 

function debug($var){
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
	die();
}

function rupiah($angka){	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
}


function change_format_tanggal($oldFormat)
{
	return date("d-m-Y h:i", strtotime($oldFormat)); // datae d-m-Y
}

function generate_number ($len = 10)
{
	$chars = array(
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
		'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
		'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
	);
	shuffle($chars);
	$num_chars = count($chars) - 1;
	$id = '';
	for ($i = 0; $i < $len; $i++)
	{
		$id .= $chars[mt_rand(0, $num_chars)];
	}
	return $id;
}

?>

