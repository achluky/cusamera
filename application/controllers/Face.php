<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Face extends CI_Controller {
    
    public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in')==NULL) {
            redirect(base_url(), 'location', 303);
		}
    }
    
	public function index()
	{
		$this->load->model('model_face');
		$data['data'] = $this->model_face->data();
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('face')
				->view('template/footer');
    }
    
    public function detail($id){
		$this->load->model('model_face');
		$data['data'] = $this->model_face->detail($id);
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('face_detail')
				->view('template/footer');
    }
    
    public function delete($id){
		$this->load->model('model_face');
		$this->model_face->delete($id);
        redirect($this->config->item('base_url').'device?info=Berhasil-dihapus', 'refresh');
    }
}
