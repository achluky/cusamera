<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in')==NULL) {
            redirect(base_url(), 'location', 303);
		}
		$this->load->model('Model_profil');
	}
	public function index()
	{
		$data['profil'] = $this->Model_profil->get();
		if($this->session->logged_in['type']==1) // 1 = toko
		$view = "profil";
		else
		$view = "profil_reseller";
		$this->load->view('themplate/header', $data)
		->view($view)
		->view('themplate/footer');
	}
	public function save(){
		$data = $this->input->post();
		//upload image
		$file_name = $_FILES['image']['name'];
		$config['allowed_types'] = 'jpg|JPG|png|PNG|jpeg';
		$config['upload_path']  = "./assert/img/";
		$config['max_size'] = 1000;
		$config['file_name'] = $file_name;
		$this->load->library('upload',$config);        
		// upload end
		if (!$this->upload->do_upload('image')) {
			$msg = ". Foto Identitas: ".$this->upload->display_errors();
		}else{
			$msg = 'Foto: Berhasil Diupload';
			$file_data = $this->upload->data();
			if($file_data['file_name']!=NULL){
				$data['foto'] = $file_data['orig_name'];
			}
		}
		$rst =$this->Model_profil->save($data);
		if($rst){
			$msg = url_title("Data: Berhasil Disimpan. ".$msg );
		}else{
			$msg = url_title("Data: Gagal Disimpan. ".$msg );
		}
        redirect('/profil?info='.$msg);
	}
}
