<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index(){
		if ($this->session->userdata('logged_in')!=NULL) {
			redirect(base_url().'dashboard', 'location', 303);
		}
		$this->load->view('login');
	}
	public function act(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$this->load->model('model_login');
		$rst_auth = $this->model_login->auth($username, $password);
		if ($rst_auth[0]) {
		    $sess_array = array(
              'username' => $username,
              'id_login' => $rst_auth[1]->id_login,
			  'login_status' => TRUE,
			  'type' => $rst_auth[1]->type
            );
			$this->session->set_userdata('logged_in', $sess_array);
			redirect("/dashboard","refresh");
		} else {
			$info = url_title($rst_auth[1]);
            redirect($this->config->item('base_url').'welcome?info='.$info, 'refresh');
		}
	}
	public function break_login(){
        $sess = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            $this->session->sess_destroy();
            redirect($this->config->item('base_url').'', 'refresh');
        }else{
            redirect($this->config->item('base_url').'dashboard', 'refresh');
        }
	}

	
}
