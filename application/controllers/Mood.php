<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mood extends CI_Controller {
    
    public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in')==NULL) {
            redirect(base_url(), 'location', 303);
		}
    }
    
	public function index()
	{
		$this->load->model('model_mood');
		$data['data'] = $this->model_mood->data();
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('mood')
				->view('template/footer');
    }
    
    public function detail($code){
		$this->load->model('model_mood');
		$data['data'] = $this->model_mood->detail($code);
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('mood_detail')
				->view('template/footer');
    }
    
    public function delete($code){
		$this->load->model('model_mood');
		$this->model_mood->delete($code);
        redirect($this->config->item('base_url').'device?info=Berhasil-dihapus', 'refresh');
    }
}
