<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Device extends CI_Controller {
    
    public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in')==NULL) {
            redirect(base_url(), 'location', 303);
		}
    }
    
	public function index()
	{
		$this->load->model('model_device');
		$data['data'] = $this->model_device->data();
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('device')
				->view('template/footer');
    }
    
    public function detail($code){
		$this->load->model('model_device');
		$data['data'] = $this->model_device->detail($code);
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('device_detail')
				->view('template/footer');
    }
    
    public function delete($code){
		$this->load->model('model_device');
		$this->model_device->delete($code);
        redirect($this->config->item('base_url').'device?info=Berhasil-dihapus', 'refresh');
    }
}
