<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
        public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('logged_in')==NULL) {
                        redirect(base_url(), 'location', 303);
		}
	}
	public function index()
	{
		$this->load->model('model_dashboard');
		$data['device'] = $this->model_dashboard->device();
		$data['face'] = $this->model_dashboard->face();
		$data['mood'] = $this->model_dashboard->mood();
		$data['data'] = $this->model_dashboard->data();
		$data['bar'] = $this->model_dashboard->bar();
		$data['js'] = 'dashboard';
		$this->load->view('template/header', $data)
				->view('template/menu')
				->view('dashboard')
				->view('template/footer');
	}
}
