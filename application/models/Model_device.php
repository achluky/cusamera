<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_device extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    
    public function data(){
      $result = $this->db->query("SELECT * FROM devices");
      return $result;
    }

    public function detail($code){
      $result = $this->db->query("SELECT * FROM devices WHERE code='".$code."'");
      return $result->row();
    }

    public function delete($code){
      return $this->db->query("DELETE FROM devices WHERE code='".$code."'");
    }
}

