<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_dashboard extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    public function device(){
      $result = $this->db->query("SELECT * FROM devices ");
      return $result->num_rows();
    }
    
    public function face(){
      $result = $this->db->query("SELECT * FROM faces ");
      return $result->num_rows();
    }
    
    public function mood(){
      $result = $this->db->query("SELECT * FROM satisfaction ");
      return $result->num_rows();
    }
    
    public function data(){
      $result = $this->db->query("SELECT
                                    devices.`code`,
                                    faces.id,
                                    faces.`Name`,
                                    satisfaction.mood,
                                    satisfaction.created
                                  FROM
                                    satisfaction
                                    JOIN faces ON satisfaction.face_id = faces.id
                                    JOIN devices ON satisfaction.device_code = devices.`code`
                                  LIMIT 
                                    10");
      return $result;
    }


    
    public function bar(){
      $result = $this->db->query("SELECT
                                    devices.`code`,
                                    faces.id,
                                    faces.`Name`,
                                    satisfaction.mood,
                                    satisfaction.created
                                  FROM
                                    satisfaction
                                    JOIN faces ON satisfaction.face_id = faces.id
                                    JOIN devices ON satisfaction.device_code = devices.`code`
                                  LIMIT 
                                    10");
      return $result;
    }

}
