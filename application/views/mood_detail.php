
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-camera"></i> Data Device </h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card m-b-30">
                            <div class="card-body">
                            <div class="">
                                <img class="img-thumbnail" src="<?php echo base_url()?>assets/images/esp32.jpeg" data-holder-rendered="true">
                            </div>
                            <br/>
                            <p>Device Code : <?php echo $data->code?></p>
                            <p>Device Name : <?php echo ($data->name=='')? 'undefined':$data->name     ?></p>
                            <p>Create Date : <?php echo $data->created?></p>
                            <a href="<?php echo base_url() ?>device" class="btn btn-info btn-sm"><i class="fa fa-angle-double-left"></i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

