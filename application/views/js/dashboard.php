<script>
!function ($) {
    "use strict";
    var Dashboard = function () {
    };
    
    //creates area chart
    Dashboard.prototype.createAreaChart = function (element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 0,
            lineWidth: 0,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            resize: true,
            gridLineColor: '#eee',
            hideHover: 'auto',
            lineColors: lineColors,
            fillOpacity: 0.7,
            behaveLikeLine: true
        });
    },

    //creates area chart
    Dashboard.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            gridLineColor: '#eef0f2',
            barSizeRatio: 0.4,
            resize: true,
            hideHover: 'auto',
            barColors: lineColors
        });
    },

    //creates Donut chart
    Dashboard.prototype.createDonutChart = function (element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true,
            colors: colors,
        });
    },

    //donut
    $('.peity-donut').each(function () {
        $(this).peity("donut", $(this).data());
    });

    //pie
    $('.peity-pie').each(function () {
        $(this).peity("pie", $(this).data());
    });

    Dashboard.prototype.init = function () {
        //creating bar chart
        var $barData = [
            {y: '2019', Device: 1, Face: 4, Mood_Detection: 8},
        ];
        this.createBarChart('morris-bar-example', $barData, 'y', ['Device', 'Face', 'Mood_Detection'], ['Device', 'Face', 'Mood_Detection'], ['#508aeb', '#fcc24c', '#508aeb']);
        
        //creating donut chart
        var $donutData = [
            {label: "CALM", value: 7},
            {label: "HAPPY", value: 1},
        ];
        this.createDonutChart('morris-donut-example', $donutData, ['#54cc96', "#508aeb", '#ff5560', '#fcc24c']);
    },
    //init
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Dashboard.init();
    }(window.jQuery);

</script>