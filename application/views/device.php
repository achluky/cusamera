
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-camera"></i> Data Device </h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="table-responsive">
                                    
                                <?php if( isset($_GET['info']) and $_GET['info']!="" ){ ?>
                                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                <strong>Informasi!</strong> <?php echo str_replace("-"," ", $_GET['info']); ?>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <?php } ?>
                                    <table class="table table-dark mb-0">
                                        <thead>
                                            <tr>
                                                <th>Device Code</th>
                                                <th>Name</th>
                                                <th>Date Create</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($data->result() as $row)
                                                {
                                            ?>
                                            <tr>
                                                <td><?= $row->code ?></td>
                                                <td><?= ($row->name==NULL)?'undefined':$row->name ?></td>
                                                <td><?= $row->created ?></td>
                                                <td><a href="<?= base_url()?>device/detail/<?= $row->code ?>" class="btn btn-info btn-sm"><i class="fa fa-search-plus"></i> detail</a>&nbsp; <a href="<?= base_url()?>device/delete/<?= $row->code ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> delete</a></td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

