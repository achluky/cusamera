
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-camera"></i> Face Data </h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card m-b-30">
                            <div class="card-body">
                            <div class="">
                                <img class="img-thumbnail" src="<?php echo base_url()?>assets/images/face.jpg" data-holder-rendered="true">
                            </div>
                            <br/>
                            <p>Face Id : <?php echo $data->id?></p>
                            <p>Face Name : <?php echo ($data->Name=='')? 'undefined':$data->Name ?></p>
                            <p>Create Date : <?php echo $data->created?></p>
                            <a href="<?php echo base_url() ?>face" class="btn btn-info btn-sm"><i class="fa fa-angle-double-left"></i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

