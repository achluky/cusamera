
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-camera"></i> Mood Data</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="table-responsive">
                                    
                                <?php if( isset($_GET['info']) and $_GET['info']!="" ){ ?>
                                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                <strong>Informasi!</strong> <?php echo str_replace("-"," ", $_GET['info']); ?>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <?php } ?>
                                    <table class="table table-dark mb-0">
                                        <thead>
                                            <tr>
                                                <th>Device Name</th>
                                                <th>Mood</th>
                                                <th>Face Id</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($data->result() as $row)
                                                {
                                            ?>
                                            <tr>
                                                <td><?= $row->device_code ?></td>
                                                <td><?= $row->mood ?></td>
                                                <td><a href="<?php echo base_url() ?>face/detail/<?= $row->face_id ?>" class="btn btn-info btn-sm"><i class="fa fa-smile-o"></i> Detail Face</a></td>
                                                <td>
                                                    <a href="<?= base_url()?>mood/detail/<?= $row->id ?>" class="btn btn-info btn-sm"><i class="fa fa-search-plus"></i> Detail</a>&nbsp; 
                                                    <a href="<?= base_url()?>mood/delete/<?= $row->id ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a></td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

