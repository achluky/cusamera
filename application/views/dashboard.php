
        <div class="wrapper">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card mini-stat m-b-30">
                            <div class="p-3 bg-primary text-white">
                                <div class="mini-stat-icon">
                                    <i class="fa fa-camera float-right mb-0"></i>
                                </div>
                                <h6 class="text-uppercase mb-0">All Device</h6>
                            </div>
                            <div class="card-body">
                                <div class="mt-4 text-muted">
                                    <div class="float-right">
                                        <p class="m-0"><a href="<?= base_url()?>device">Detail</a></p>
                                    </div>
                                    <h5 class="m-0"><?= $device ?></h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card mini-stat m-b-30">
                            <div class="p-3 bg-primary text-white">
                                <div class="mini-stat-icon">
                                    <i class="fa fa-drupal float-right mb-0"></i>
                                </div>
                                <h6 class="text-uppercase mb-0">All Face Data</h6>
                            </div>
                            <div class="card-body">
                                <div class="mt-4 text-muted">
                                    <div class="float-right">
                                        <p class="m-0"><a href="<?= base_url()?>face">Detail</a></p>
                                    </div>
                                    <h5 class="m-0"><?= $face ?></h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card mini-stat m-b-30">
                            <div class="p-3 bg-primary text-white">
                                <div class="mini-stat-icon">
                                    <i class="fa fa-smile-o float-right mb-0"></i>
                                </div>
                                <h6 class="text-uppercase mb-0">Total Mood Data</h6>
                            </div>
                            <div class="card-body">
                                <div class="mt-4 text-muted">
                                    <div class="float-right">
                                        <p class="m-0"><a href="<?= base_url()?>mood">Detail</a></p>
                                    </div>
                                    <h5 class="m-0"><?= $mood ?></h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-xl-4">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <h4 class="mt-0 header-title mb-4">Data Device, Face and Mood Detection</h4>
                                <div id="morris-bar-example" style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <h4 class="mt-0 header-title mb-4">Persentation Mood All Device</h4>
                                <div id="morris-donut-example" style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <h4 class="mt-0 header-title mb-4">Recent Data Collection</h4>
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th>Device Code</th>
                                                <th>Face Id</th>
                                                <th>Face Name</th>
                                                <th>Mood</th>
                                                <th>Date Create</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($data->result() as $row)
                                                {
                                            ?>
                                            <tr>
                                                <td><?= $row->code ?></td>
                                                <td><?= $row->id ?></td>
                                                <td><?= ($row->Name==NULL)?'undefined':$row->Name ?></td>
                                                <td><?= $row->mood ?></td>
                                                <td><?= $row->created ?></td>
                                            </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

