/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50539
 Source Host           : localhost:3306
 Source Schema         : cusamera

 Target Server Type    : MySQL
 Target Server Version : 50539
 File Encoding         : 65001

 Date: 14/12/2019 05:15:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for devices
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of devices
-- ----------------------------
BEGIN;
INSERT INTO `devices` VALUES (1, 'ESP-91', 'Hercules', '2019-12-14 02:02:09');
COMMIT;

-- ----------------------------
-- Table structure for faces
-- ----------------------------
DROP TABLE IF EXISTS `faces`;
CREATE TABLE `faces` (
  `id` char(36) NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of faces
-- ----------------------------
BEGIN;
INSERT INTO `faces` VALUES ('e3897b89-1f65-4745-ba7d-355dd2d9b41f', NULL, '2019-10-18 07:40:09');
INSERT INTO `faces` VALUES ('e5141a1f-a9e3-48f2-a348-acd65ec02b92', NULL, '2019-10-18 07:38:34');
INSERT INTO `faces` VALUES ('f3e4a3c0-a1cf-4f5a-bcc7-abb5302ebcaf', NULL, '2019-10-18 07:35:09');
INSERT INTO `faces` VALUES ('f6f08120-9991-488e-bca0-3640c3af2580', NULL, '2019-10-18 07:31:42');
COMMIT;

-- ----------------------------
-- Table structure for satisfaction
-- ----------------------------
DROP TABLE IF EXISTS `satisfaction`;
CREATE TABLE `satisfaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_code` varchar(10) DEFAULT NULL,
  `face_id` char(36) NOT NULL,
  `mood` varchar(30) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of satisfaction
-- ----------------------------
BEGIN;
INSERT INTO `satisfaction` VALUES (20, 'ESP-91', '0fdc6380-cb10-4995-8fc7-873ef602038a', 'CALM', '2019-10-18 07:15:11');
INSERT INTO `satisfaction` VALUES (21, 'ESP-91', '0fdc6380-cb10-4995-8fc7-873ef602038a', 'CALM', '2019-10-18 07:18:32');
INSERT INTO `satisfaction` VALUES (22, 'ESP-91', 'f6f08120-9991-488e-bca0-3640c3af2580', 'CALM', '2019-10-18 07:31:42');
INSERT INTO `satisfaction` VALUES (23, 'ESP-91', 'f3e4a3c0-a1cf-4f5a-bcc7-abb5302ebcaf', 'CALM', '2019-10-18 07:35:09');
INSERT INTO `satisfaction` VALUES (24, 'ESP-91', '6b25db69-f87e-4fe9-9467-3b8cfa304a16', 'CALM', '2019-10-18 07:36:42');
INSERT INTO `satisfaction` VALUES (25, 'ESP-91', 'e5141a1f-a9e3-48f2-a348-acd65ec02b92', 'CALM', '2019-10-18 07:38:34');
INSERT INTO `satisfaction` VALUES (26, 'ESP-91', 'e5141a1f-a9e3-48f2-a348-acd65ec02b92', 'CALM', '2019-10-18 07:38:41');
INSERT INTO `satisfaction` VALUES (27, 'ESP-91', 'e3897b89-1f65-4745-ba7d-355dd2d9b41f', 'HAPPY', '2019-10-18 07:40:09');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_login` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_login`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'admin', 'c93ccd78b2076528346216b3b2f701e6', 1, '2019-02-02 00:00:00', '1', 'xyz2387688szxt');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
